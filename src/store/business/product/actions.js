import { axiosInstance } from 'boot/axios'

export const getAll = async ({ commit }) => {
  try {
    const { data: resp } = await axiosInstance.get('business/product')
    const { data } = resp
    commit('ALL', data)
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const save = async ({ commit }, product) => {
  try {
    const { data: resp } = await axiosInstance.post(`business/product`, product)
    const { data, message } = resp
    commit('SAVE', data)
    return message
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const edit = async ({ commit }, product) => {
  const { data: resp } = await axiosInstance.get(`business/product/${product}`)
  return resp
}

export const update = async ({ commit }, { id, product, index }) => {
  try {
    const { data, message } = await axiosInstance.put(`business/product/${id}`, product).then((resp) => resp.data)
    commit('UPDATE', { data, index })
    return message
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const destroy = async ({ commit }, { id, index }) => {
  try {
    const query = await axiosInstance.delete(`business/product/${id}`).then(resp => resp.data)
    commit('DESTROY', index)
    return query
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}
