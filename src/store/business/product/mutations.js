export const ALL = (state, products) => { state.products = products }
export const SAVE = (state, product) => state.products.unshift(product)
export const UPDATE = (state, { data, index }) => {
  state.products[index].price = data.price.toFixed(2)
}
export const DESTROY = (state, index) => state.products.splice(index, 1)
