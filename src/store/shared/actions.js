import { axiosInstance } from 'boot/axios'

export const getCountries = async ({ commit }) => {
  const { data: resp } = await axiosInstance.get('shared/countries')
  const { data: countries } = resp
  commit('SETCOUNTRIES', countries)
}

export const getCities = async ({ commit }, country) => {
  const { data: resp } = await axiosInstance.get(`shared/countries/${country}/cities`)
  const { data: cities } = resp
  commit('SETCITIES', cities)
}

export const getBrands = async ({ commit }, country = null) => {
  const { data: resp } = await axiosInstance.get(`shared/brands`, { params: { country } })
  const { data: brands } = resp
  commit('SETBRANDS', brands)
}

export const getProducts = async ({ commit }, { country = '', brand }) => {
  const { data: resp } = await axiosInstance.get(`shared/brand/${brand}/products`, { params: { country } })
  const { data: products } = resp
  commit('SETPRODUCTS', products)
}

export const getPresentations = async ({ commit }, { product }) => {
  const { data: resp } = await axiosInstance.get(`shared/product/${product}/presentations`)
  const { data: presentations } = resp
  commit('SETPRESENTATIONS', presentations)
}
