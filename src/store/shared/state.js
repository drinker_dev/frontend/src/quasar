export default {
  countries: [],
  countryOptions: [],
  cities: [],
  cityOptions: [],
  brands: [],
  brandOptions: [],
  products: [],
  productOptions: [],
  presentations: [],
  presentationOptions: []
}
