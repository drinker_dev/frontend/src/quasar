export const SETCOUNTRIES = (state, countries) => {
  state.countries = countries.map(country => { return { label: country.name, value: { name: country.name, _id: country._id, geoLocation: country.geoLocation, code: country.code } } })
  state.countryOptions = JSON.parse(JSON.stringify(state.countries))
}

export const SEARCHCOUNTRIES = (state, search) => {
  if (search.length > 0) {
    state.countryOptions = state.countries.filter(v => v.label.toLowerCase().indexOf(search.toLowerCase()) > -1)
  } else {
    state.countryOptions = JSON.parse(JSON.stringify(state.countries))
  }
}

export const SETCITIES = (state, cities) => {
  state.cities = cities.map(city => { return { label: city.name, value: { name: city.name, _id: city._id, geoLocation: city.geoLocation } } })
  state.cityOptions = JSON.parse(JSON.stringify(state.cities))
}

export const SEARCHCITIES = (state, search) => {
  if (search.length > 0) {
    state.cityOptions = state.cities.filter(v => v.label.toLowerCase().indexOf(search.toLowerCase()) > -1)
  } else {
    state.cityOptions = JSON.parse(JSON.stringify(state.cities))
  }
}

export const SETBRANDS = (state, brands) => {
  state.brands = brands.map(brand => { return { label: brand.name, value: { name: brand.name, _id: brand._id } } })
  state.brandOptions = JSON.parse(JSON.stringify(state.brands))
}

export const SEARCHBRANDS = (state, search) => {
  if (search.length > 0) {
    state.brandOptions = state.brands.filter(v => v.label.toLowerCase().indexOf(search.toLowerCase()) > -1)
  } else {
    state.brandOptions = JSON.parse(JSON.stringify(state.brands))
  }
}

export const SETPRODUCTS = (state, products) => {
  state.products = products.map(product => { return { label: product.name, value: { name: product.name, _id: product._id } } })
  state.productOptions = JSON.parse(JSON.stringify(state.products))
}

export const SEARCHPRODUCTS = (state, search) => {
  if (search.length > 0) {
    state.productOptions = state.products.filter(v => v.label.toLowerCase().indexOf(search.toLowerCase()) > -1)
  } else {
    state.productOptions = JSON.parse(JSON.stringify(state.products))
  }
}

export const SETPRESENTATIONS = (state, presentations) => {
  state.presentations = presentations
  state.presentationOptions = JSON.parse(JSON.stringify(state.presentations))
}

export const SEARCHPRESENTATIONS = (state, search) => {
  if (search.length > 0) {
    state.presentationOptions = state.presentations.filter(v => v.label.toLowerCase().indexOf(search.toLowerCase()) > -1)
  } else {
    state.presentationOptions = JSON.parse(JSON.stringify(state.presentations))
  }
}
