export const countryOptions = (state) => state.countryOptions
export const cityOptions = (state) => state.cityOptions
export const brandOptions = (state) => state.brandOptions
export const productOptions = (state) => state.productOptions
export const presentationOptions = (state) => state.presentationOptions
