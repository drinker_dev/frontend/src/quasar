export const ALL = (state, brands) => { state.brands = brands }
export const SAVE = (state, brand) => state.brands.unshift(brand)
export const UPDATE = (state, { data, index }) => {
  state.brands[index].name = data.name
  state.brands[index].country = data.country
  state.brands[index].enabled = data.enabled
}
export const DESTROY = (state, index) => state.brands.splice(index, 1)
