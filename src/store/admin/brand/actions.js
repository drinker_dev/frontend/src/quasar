import { axiosInstance } from 'boot/axios'

export const getAll = async ({ commit }) => {
  try {
    const query = await axiosInstance.get('admin/brand').then((resp) => resp.data)
    commit('ALL', query)
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const save = async ({ commit }, brand) => {
  try {
    const { data, message } = await axiosInstance.post(`admin/brand`, brand).then((resp) => resp.data)
    commit('SAVE', data)
    return message
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const edit = ({ commit }, brand) => {
  return axiosInstance.get(`admin/brand/${brand}`).then((resp) => resp.data.data)
}

export const update = async ({ commit }, { id, brand, index }) => {
  try {
    const { data, message } = await axiosInstance.put(`admin/brand/${id}`, brand).then((resp) => resp.data)
    commit('UPDATE', { data, index })
    return message
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const destroy = async ({ commit }, { id, index }) => {
  try {
    const query = await axiosInstance.delete(`admin/brand/${id}`).then(resp => resp.data)
    commit('DESTROY', index)
    return query
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}
