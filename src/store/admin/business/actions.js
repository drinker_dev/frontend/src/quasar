import { axiosInstance } from 'boot/axios'

export const getAll = async ({ commit }) => {
  try {
    const query = await axiosInstance.get('admin/business').then((resp) => resp.data)
    commit('ALL', query)
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const save = async ({ commit }, business) => {
  try {
    const { data, message } = await axiosInstance.post(`admin/business`, business).then((resp) => resp.data)
    commit('SAVE', data)
    return message
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const edit = ({ commit }, business) => {
  return axiosInstance.get(`admin/business/${business}/edit`).then((resp) => resp.data)
}

export const updateCondition = async ({ commit }, { id, business, index }) => {
  try {
    const { data, message } = await axiosInstance.put(`admin/business/${id}/condition`, business).then((resp) => resp.data)
    commit('UPDATECONDITION', { data, index })
    return message
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const update = async ({ commit }, { id, business, index = null }) => {
  let data, message
  try {
    ({ data, message } = await axiosInstance.put(`admin/business/${id}`, business).then((resp) => resp.data))
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }

  if (index != null) {
    commit('UPDATE', { data, index })
    return message
  }
  return { data, message }
}

export const destroy = async ({ commit }, { id, index }) => {
  try {
    const query = await axiosInstance.delete(`admin/business/${id}`).then(resp => resp.data)
    commit('DESTROY', index)
    return query
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}
