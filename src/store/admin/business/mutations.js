export const ALL = (state, business) => { state.business = business }
export const SAVE = (state, business) => state.business.unshift(business)
export const UPDATECONDITION = (state, { data, index }) => {
  state.business[index].status = data.status
  state.business[index].user.enabled = data.user.enabled
}
export const UPDATE = (state, { data, index }) => {
  state.business[index].name = data.name
  state.business[index].rif = data.rif
  state.business[index].country = data.country
  state.business[index].city = data.city
  state.business[index].address = data.address
  state.business[index].geoLocation = data.geoLocation
}
export const DESTROY = (state, index) => state.business.splice(index, 1)
