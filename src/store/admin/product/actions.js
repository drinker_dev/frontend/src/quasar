import { axiosInstance } from 'boot/axios'

export const getAll = async ({ commit }) => {
  try {
    const query = await axiosInstance.get('admin/product').then((resp) => resp.data)
    commit('ALL', query)
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const save = async ({ commit }, product) => {
  try {
    const { data, message } = await axiosInstance.post(`admin/product`, product).then((resp) => resp.data)
    commit('SAVE', data)
    return message
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const edit = ({ commit }, product) => {
  return axiosInstance.get(`admin/product/${product}`).then((resp) => resp.data.data)
}

export const update = async ({ commit }, { id, product, index }) => {
  try {
    const { data, message } = await axiosInstance.put(`admin/product/${id}`, product).then((resp) => resp.data)
    commit('UPDATE', { data, index })
    return message
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const destroy = async ({ commit }, { id, index }) => {
  try {
    const query = await axiosInstance.delete(`admin/product/${id}`).then(resp => resp.data)
    commit('DESTROY', index)
    return query
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}
