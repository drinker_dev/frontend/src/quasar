export const ALL = (state, products) => { state.products = products }
export const SAVE = (state, brand) => state.products.unshift(brand)
export const UPDATE = (state, { data, index }) => {
  state.products[index].country = data.country
  state.products[index].brand = data.brand
  state.products[index].name = data.name
  state.products[index].presentation = data.presentation
}
export const DESTROY = (state, index) => state.products.splice(index, 1)
