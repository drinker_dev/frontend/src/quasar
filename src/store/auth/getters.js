export const isLogin = (state) => state.token !== ''
export const type = (state) => state.user.type
export const role = (state) => state.user.role
export const fullName = (state) => {
  if (state.user.type === 'Business') {
    return state.user.profile.name
  }
  if (state.user.type === 'Admin') {
    return `${state.user.profile.firstName} ${state.user.profile.lastName}`
  }
}
export const id = (state) => state.user._id
export const country = (state) => state.user.profile.country
