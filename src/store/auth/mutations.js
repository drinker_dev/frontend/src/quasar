import jwtDecode from 'jwt-decode'

export const LOGIN = (state, token) => {
  state.token = token
  state.user = jwtDecode(token)
}

export const LOGOUT = (state) => {
  state.token = ''
  state.user = {}
}
