import jwtDecode from 'jwt-decode'

const token = localStorage.getItem('token') || ''

export default {
  user: token ? jwtDecode(token) : {},
  token
}
