import { axiosInstance } from 'boot/axios'

export const login = async ({ commit }, credentials) => {
  try {
    const { data } = await axiosInstance.post('login', credentials)
    localStorage.setItem('token', data.token)
    axiosInstance.defaults.headers['Authorization'] = `Bearer ${data.token}`
    commit('LOGIN', data.token)
  } catch (err) {
    // console.error(err)
    throw err.response.data
  }
}

export const logout = ({ commit }) => {
  localStorage.removeItem('token')
  axiosInstance.defaults.headers['Authorization'] = `Bearer `
  commit('LOGOUT')
}
