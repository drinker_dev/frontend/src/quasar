import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import shared from './shared'
import adminBrand from './admin/brand'
import adminBusiness from './admin/business'
import adminProduct from './admin/product'
import businessProduct from './business/product'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      auth,
      shared,
      adminBusiness,
      adminProduct,
      adminBrand,
      businessProduct
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
