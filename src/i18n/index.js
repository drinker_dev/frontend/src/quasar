import enUS from './en-us'
import esVE from './es-ve'

export default {
  'en-us': enUS,
  'es-ve': esVE
}
