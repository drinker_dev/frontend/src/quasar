export default async ({ router, store }) => {
  router.beforeEach((to, from, next) => {
    // valida si las rutas solicitadas requieren autenticación
    const requireAuth = to.matched.some(record => record.meta.requireAuth)
    // valida las rutas que no requieren autentincación
    const notRequireAuth = !requireAuth
    // valida que las rutas requieran user type Admin
    const requireUserTypeAdmin = to.matched.some(record => record.meta.requireUserTypeAdmin)
    // valida que las rutas requieran user type Business
    const requireUserTypeBusiness = to.matched.some(record => record.meta.requireUserTypeBusiness)
    // valida si hay un token en localStore
    const isLogin = store.getters['auth/isLogin']

    switch (store.getters['auth/type']) {
      case 'Business':
        if (isLogin && (notRequireAuth || !requireUserTypeBusiness)) {
          next({ name: 'business.index' })
        } else {
          next()
        }
        break
      case 'Admin':
        if (isLogin && (notRequireAuth || !requireUserTypeAdmin)) {
          next({ name: 'admin.index' })
        } else {
          next()
        }
        break
      default:
        if (requireAuth && !isLogin) {
          next({ name: 'auth.login' })
        }
        if (notRequireAuth) {
          next()
        }
        break
    }
  })
}
