import * as VueGoogleMaps from 'vue2-google-maps'

export default async ({ Vue }) => {
  Vue.use(VueGoogleMaps, {
    load: {
      key: process.env.MAP_KEY,
      libraries: 'places'
    }
  })
}
