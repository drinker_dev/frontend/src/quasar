import _ from 'eslint-loader'

// "async" is optional
export default () => {
  window._ = _
}
