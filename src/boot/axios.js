import Vue from 'vue'
import axios from 'axios'

const token = localStorage.getItem('token')

const axiosInstance = axios.create({
  baseURL: process.env.NODE_ENV === 'production' ? process.env.API_PRO : process.env.API_DEV,
  headers: { Authorization: `Bearer ${token}` }
})

Vue.prototype.$axios = axiosInstance

export { axiosInstance }
