const routes = [
  {
    path: '/',
    redirect: 'auth/login'
  },
  {
    path: '/auth',
    component: () => import('layouts/auth/Auth.vue'),
    children: [
      {
        path: '',
        redirect: 'login'
      },
      {
        path: 'login',
        component: () => import('pages/auth/Login.vue'),
        name: 'auth.login'
      },
      {
        path: 'register',
        component: () => import('pages/auth/register/Index.vue'),
        children: [
          {
            path: '',
            redirect: 'business'
          },
          {
            path: 'business',
            name: 'auth.register.business',
            component: () => import('pages/auth/register/Business.vue')
          }
        ]
      }
    ]
  },
  {
    path: '/verify/:token/user/:id',
    component: () => import('layouts/auth/Auth.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/auth/Verification.vue'),
        props: true
      }
    ]
  },
  {
    path: '/admin',
    name: 'admin.index',
    component: () => import('layouts/admin/Admin.vue'),
    meta: {
      requireAuth: true,
      requireUserTypeAdmin: true
    },
    children: [
      {
        path: 'business',
        name: 'admin.business',
        component: () => import('pages/admin/business/Index.vue'),
        children: [
          {
            path: 'create',
            name: 'admin.business.create',
            component: () => import('pages/admin/business/Create.vue')
          },
          {
            path: ':business_id/edit',
            name: 'admin.business.edit',
            component: () => import('pages/admin/business/Edit.vue'),
            props: true
          },
          {
            path: ':business_id/condition',
            name: 'admin.business.condition',
            component: () => import('pages/admin/business/Condition.vue'),
            props: true
          }
        ]
      },
      {
        path: 'article',
        component: () => import('pages/admin/article/Index.vue'),
        children: [
          {
            path: '',
            redirect: 'product'
          },
          {
            path: 'brand',
            name: 'admin.article.brand',
            component: () => import('pages/admin/article/brand/Index.vue'),
            children: [
              {
                path: 'brand',
                name: 'admin.article.brand.create',
                component: () => import('pages/admin/article/brand/Create.vue')
              },
              {
                path: ':brand_id/edit',
                name: 'admin.article.brand.edit',
                component: () => import('pages/admin/article/brand/Edit.vue'),
                props: true
              }
            ]
          },
          {
            path: 'product',
            name: 'admin.article.product',
            component: () => import('pages/admin/article/product/Index.vue'),
            children: [
              {
                path: 'create',
                name: 'admin.article.product.create',
                component: () => import('pages/admin/article/product/Create.vue')
              },
              {
                path: ':product_id/edit',
                name: 'admin.article.product.edit',
                component: () => import('pages/admin/article/product/Edit.vue'),
                props: true
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/business',
    name: 'business.index',
    component: () => import('layouts/users/Business.vue'),
    meta: {
      requireAuth: true,
      requireUserTypeBusiness: true
    },
    children: [
      {
        path: 'profile',
        name: 'business.profile',
        component: () => import('pages/user/business/Profile.vue')
      },
      {
        path: 'product',
        name: 'business.product',
        component: () => import('pages/user/business/product/Index.vue'),
        children: [
          {
            path: 'create',
            name: 'business.product.create',
            component: () => import('pages/user/business/product/Create.vue')
          },
          {
            path: ':product_id/edit',
            name: 'business.product.edit',
            component: () => import('pages/user/business/product/Edit.vue'),
            props: true
          }
        ]
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
